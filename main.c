#include "stm32f4xx.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_bus.h"


int DataMain  = 6;

void SPI5_IRQHandler(void)
{ 
  DataMain  = LL_SPI_ReceiveData16(SPI5);
}

int main()
{
  
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SPI5);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF); //for SPI1_NSS/SCK/MOSI/MISO
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
  
  LL_GPIO_SetPinMode(GPIOF,LL_GPIO_PIN_7,LL_GPIO_MODE_ALTERNATE); // set alternative functions for chosen pins
  LL_GPIO_SetPinMode(GPIOF,LL_GPIO_PIN_8,LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinMode(GPIOF,LL_GPIO_PIN_9,LL_GPIO_MODE_ALTERNATE);
  
  LL_GPIO_SetAFPin_0_7(GPIOF, LL_GPIO_PIN_7, LL_GPIO_AF_5);// choose finctions (5)
  LL_GPIO_SetAFPin_8_15(GPIOF, LL_GPIO_PIN_8, LL_GPIO_AF_5);
  LL_GPIO_SetAFPin_8_15(GPIOF, LL_GPIO_PIN_9, LL_GPIO_AF_5);
  
  GPIOC->MODER |= GPIO_MODER_MODER1_0; // Set pin of NSS output (use hal_ll)
  
  
  LL_SPI_SetMode(SPI5, LL_SPI_MODE_MASTER);
  LL_SPI_SetClockPolarity(SPI5,LL_SPI_POLARITY_HIGH);// 
  LL_SPI_SetClockPhase(SPI5, LL_SPI_PHASE_2EDGE); // rising front
  LL_SPI_SetBaudRatePrescaler(SPI5, LL_SPI_BAUDRATEPRESCALER_DIV4);
  LL_SPI_SetTransferBitOrder(SPI5, LL_SPI_MSB_FIRST);
  LL_SPI_SetDataWidth(SPI5, LL_SPI_DATAWIDTH_16BIT);
  LL_SPI_SetTransferDirection(SPI5, LL_SPI_FULL_DUPLEX);
  LL_SPI_SetNSSMode(SPI5, LL_SPI_NSS_HARD_OUTPUT);// 
  
  LL_SPI_EnableIT_RXNE(SPI5); // if Rx buffer not empty enable interrupt
 
  NVIC_EnableIRQ(SPI5_IRQn);// enable interrupts
  
  GPIOC->ODR |= GPIO_ODR_OD1; //NSS in "1"
  
  //LL_SPI_ReceiveData16(SPI5);
  LL_SPI_Enable(SPI5);
  
  int adress = 0x8F00; //10 00 1111 0000 0000
  int tempAdress = 0xA900;
  int Gyro = 0x200F; //0b 0 0 10 0000 0000 1111
  GPIOC->ODR &= ~GPIO_ODR_OD1;
  LL_SPI_TransmitData16(SPI5,Gyro);
  GPIOC->ODR |= GPIO_ODR_OD1;
  while(1)
  {  
    GPIOC->ODR &= ~GPIO_ODR_OD1;
    LL_SPI_TransmitData16(SPI5, adress);
   // LL_SPI_TransmitData16(SPI5, tempAdress);
    GPIOC->ODR |= GPIO_ODR_OD1;
    for (int i = 0; i<10; i++);
   
    
  }
}
